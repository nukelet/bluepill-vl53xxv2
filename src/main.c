#include <stdio.h>

#include <device.h>         // device_get_binding()
#include <devicetree.h>     // DT macros
#include <drivers/sensor.h>

#define SENSOR DT_NODELABEL(dist_sensor)
#if !DT_NODE_HAS_STATUS(SENSOR, okay)
#error "Sensor driver is disabled!"
#endif

void main(void)
{
    const struct device *sensor = device_get_binding(DT_LABEL(SENSOR));
    struct sensor_value value;
    int ret;

    sensor = device_get_binding(DT_LABEL(SENSOR));
    if (!sensor) {
        printk("Failed to initialize distance sensor\n");
        return;
    }
    else {
        printk("Distance sensor initialized\n");
    }

    while(1) {
        ret = sensor_sample_fetch(sensor);
        if (ret) {
            printk("Sensor sample fetch failed (%d)\n", ret);
        }

        ret = sensor_channel_get(sensor, SENSOR_CHAN_DISTANCE, &value);
        if (ret) {
            printk("Sensor distance reading failed (%d)\n", ret);
        }

        printk("distance: %.3fm\n", sensor_value_to_double(&value));
        k_sleep(K_MSEC(500));
    }

    
}