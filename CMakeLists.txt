cmake_minimum_required(VERSION 3.20.0)

set(CMAKE_EXPORT_COMPILE_COMMANDS 1)
set(BOARD stm32_min_dev_blue)

find_package(Zephyr)
project(bluepill-vl53lxxv2)

target_sources(app PRIVATE src/main.c)