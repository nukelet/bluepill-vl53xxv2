## Compiling and flashing:
Assuming you're using `west`, compile with:

```$ west build -b stm32_min_dev_blue```

And then flash with:

```$ west flash --config ./openocd.cfg```

If you get an error like the following:
```
Error: jtag status contains invalid mode value - communication failure
```

That means the board is on low-power mode. In order to flash it you'll need to hold the board's RESET button, then start flashing (with `west flash`); then once OpenOCD starts flashing the board, you need to release the RESET button.
